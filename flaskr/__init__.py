import os
from flask import Flask
from . import db
from . import auth
from . import blog
'''
Application factory for creating the flask app
'''

def create_app(test_config = None):
    #create app
    app = Flask(__name__, instance_relative_config = True)
    #config the app
    app.config.from_mapping(
        SECRET_KEY = os.getenv('SECRET_KEY'),
        DATABASE = os.path.join(app.instance_path, os.getenv('DATABASE_URI'))
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent = True)
    else:
        #load the test_config if pased in
        app.config.from_mapping(test_config)
    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError as e:
        print(e.args)
    #Initialize database
    db.init_app(app)
    #Register the blueprint
    app.register_blueprint(auth.bp)
    app.register_blueprint(blog.bp)
    # a simple page that says hello
    @app.route('/hello')
    def ping():
        return 'Hello world'
    return app

