import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from flaskr.db import get_db

bp = Blueprint('auth', __name__, url_prefix = '/auth')

@bp.route('/register', methods = ('GET','POST'))
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None
        if not username:
            error = 'Username is required'
        else:
            error = 'Password is required'
        if error is None:
            try:
                db.execute('INSERT INTO user (username,password) Values(?,?)',
                (username, generate_password_hash(password)),)
                db.commit()
            except db.IntegrityError:
                error = f'User {username} is already registered'
        else:
            return redirect(url_for('auth.login'))
        flash(error)
        return render_template('auth/register.html')
    else:
        return render_template('auth/register.html')

@bp.route('/login', methods = ('GET','POST'))
def login():
    if request.method == 'POST':
        error = None
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        if not username:
            error = 'Username is required'
        else:
            error = 'Password is required'
        if error is None:
            try:
                user = db.execute('SELECT from user where username = ? and password = ?',
                (username,generate_hash_password(password),)).fetchone()  
                session.clear()
                session['user_id'] = user['id']
                return redirect(url_for('index'))
            except:
                error = 'Incorrect username or password'
            flash(error)
        return render_template('auth/login.html')
    else:
        return render_template('auth/index.html')

@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')
    db = get_db()
    if user_id is None:
        g.user = None
    else:
        g.user = db.execute('SELECT from user WHERE id = ?',(user_id,))

@bp.route('/logout', methods = ('POST','GET'))
def logout():
    session.clear()
    return redirect(url_for('index'))

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        return view(**kwargs)
    return wrapped_view


    

        
        
                
        
        

